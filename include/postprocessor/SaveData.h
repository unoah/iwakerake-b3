/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SaveData.h
 * Author: unoah
 *
 * Created on December 14, 2017, 6:31 AM
 */

#ifndef SAVEDATA_H
#define SAVEDATA_H

class SaveData {
public:
    SaveData();
    SaveData(const SaveData& orig);
    virtual ~SaveData();

    int rawSensorHeader(char fileName[]);
    int rawSensorData(double rawTemperature, double rawPressure,
            double rawRakePressure, double rawDynamicPressure, char fileName[]);
    int reducedCurrentFlightHeader(char fileName[]);
    int reducedCurrentFlightData(char fileName[], int spanWiseStation, double RE,
            double currentTemperature, double currentPressure, double cd,
            double cl, double V_inf, int iterCount);
    int reducedMeanFlightHeader(char fileName[]);
    int reducedMeanFlightData(char fileName[], double reynoldsNumber, double freeStreamTemperature,
            double freeStreamPressure, double dragCoeff, double liftCoeff,
            double freeStreamVelocity);

private:
    //char testName;
    //char fileName;

    int testRunNumber;
    int iterationCount;
    int spanWiseStation;

    double freeStreamTemperature;
    double freeStreamPressure;
    double freeStreamVelocity;

    double reynoldsNumber;
    double wakeRakePressure;
    double dynamicPressure;

    double liftCoeff;
    double dragCoeff;

    //double rawSensorData;
    //double currentFlightConfigData;
    //double flightConfigHistoryData;
};

#endif /* SAVEDATA_H */

