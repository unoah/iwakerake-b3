//
// Created by unoah on 13/11/18.
//

#ifndef IWAKERAKE_SLEEPER_H
#define IWAKERAKE_SLEEPER_H

#include <QtCore/QThread>

class Sleeper : public QThread {

public:
    static void usleep(unsigned long usecs);
    static void msleep(unsigned long msecs);
    static void sleep(unsigned long secs);
};

#endif //IWAKERAKE_SLEEPER_H
