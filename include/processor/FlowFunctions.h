/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FlowFunctions.h
 * Author: unoah
 *
 * Created on February 4, 2017, 11:20 AM
 */

#ifndef FLOWFUNCTIONS_H
#define FLOWFUNCTIONS_H

class FlowFunctions {
public:
    FlowFunctions();
    FlowFunctions(const FlowFunctions& orig);
    virtual ~FlowFunctions();

    double getFreeStreamDensity(double freeStreamPressure, double freeStreamTemperature,
            double gasConst);
    double getFreeStreamVelocity(double dynamicPressure, double freeStreamDensity);
    double getSpeedOfSound(double gasConstant, double specificHeatRatio,
            double freeStreamTemperature);
    double getMachNumber(double freeStreamVelocity, double speedOfSound);
    double getReynoldsNumber(double absViscosity, double airfoilChord,
            double freeStreamDensity, double freeStreamVelocity);
    double getAbsoluteViscosity(double freeStreamTemperature);
    double getTotalPressure(double staticPressure, double dynamicPressure);

private:
    double freeStreamDensity;
    double freeStreamVelocity;
    double speedOfSound;
    double freeStreamMachNumber;
    double reynoldsNumber;
    double absoluteViscosity;
    double totalPressure;
    //double airfoilChord;
    //double dynaPressure;
    //double freeStreamPressure;
    //double freeStreamTemperature;
};

#endif /* FLOWFUNCTIONS_H */

