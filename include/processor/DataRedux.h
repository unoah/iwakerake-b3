/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DataRedux.h
 * Author: unoah
 *
 * Created on February 4, 2017, 11:16 AM
 */

#ifndef DATAREDUX_H
#define DATAREDUX_H

class DataRedux {
public:
    DataRedux();
    DataRedux(const DataRedux& orig);
    //    DataRedux(double dimensionlessPressure, double dimensionlessWakeHeight,
    //            double etaWake);
    DataRedux(double dimensionlessPressure, double zetaWake, double etaWake,
            double MachNo);
    virtual ~DataRedux();

    double getThicknessRatio();
    double getEtaWake(double RE, double TC, double Xi, double cd);
    double getZetaWake(double RE, double TC, double Xi, double cd,
            double airfoilChord);
    double getgetDimensionlessRakeHeight();
    double getDimensionlessPressure(double RE, double TC, double Xi);
    double getPressureDeficitCorrection(double etaWake, double zetaRake,
            double wakeRakePressure, double dynamicPressure);

    double getLiftCoefficient(double weight, double surfaceArea,
            double freeStreamDensity, double freeStreamVelocity);

    //double getIntegratingFactor(double y);
    double getIntegratingFactor();
    //double getIntegratingFactorFunction(double y, void *params);

    double getUncorrectedDragCoefficient(double integratingFactor,
            double dimensionlessRakeHeight, double airfoilChord,
            double pressureDeficitCorrection, double dynamicPressure);
    //    double getUncorrectedDragCoefficient(double integratingFactor,
    //            double dimensionlessRakeHeight, double airfoilChord, double totalPressure,
    //            double wakeRakePressure, double dynamicPressure);

    double getCompressibilityCorrection();


private:
    double etaWake;
    double zetaWake;
    double dimensionlessRakeHeight;
    double thicknessRatio;
    double dimensionlessPressure;
    double dimensionlessWakeHeight;
    double pressureDeficitCorrection;
    double liftCoefficient;
    double correctedDragCoefficient; // cd with commpressibility correction
    double uncorrectedDragCoefficient; // cd without commpressibility correction
    double integratingFactor;

    static double integratingFactorEquation(double y, void *params);
    static double compressibilityCorrectionEquation(double y, void *params);

};

#endif /* DATAREDUX_H */

