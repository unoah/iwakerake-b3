/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Sensors.h
 * Author: unoah
 *
 * Created on February 4, 2017, 8:16 PM
 */

#ifndef SENSORS_H
#define SENSORS_H

class Sensors {
public:
    Sensors();
    Sensors(const Sensors& orig);
    virtual ~Sensors();

    // pt0 = freeStreamPressure = dynamicPressure
    double getDynamicPressure();
    double getFreeStreamPressure();
    double getFreeStreamTemperature();
    double getWakeRakePressure();

private:
    double dummyDynamicPressure;
    double dummyWakeRakePressure;
    double dummyFreeStreamPressure;
    double dummyFreeStreamTemperature;
};

#endif /* SENSORS_H */

