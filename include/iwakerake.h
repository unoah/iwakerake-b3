/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   iwakerake.h
 * Author: unoah
 *
 * Created on December 12, 2017, 11:55 PM
 */

#ifndef IWAKERAKE_H
#define IWAKERAKE_H

#include <QMainWindow>

namespace Ui {
class iwakerake;
}

class iwakerake : public QMainWindow
{
    Q_OBJECT

public:
    explicit iwakerake(QWidget *parent = 0);
    ~iwakerake();

private slots:
    void on_takeWindOnPushButton_clicked();

private:
    Ui::iwakerake *ui;
};

#endif // IWAKERAKE_H
