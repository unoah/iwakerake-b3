/********************************************************************************
** Form generated from reading UI file 'iwakerake.ui'
**
** Created by: Qt User Interface Compiler version 5.3.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_IWAKERAKE_H
#define UI_IWAKERAKE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "qwt_plot.h"
#include "qwt_thermo.h"

QT_BEGIN_NAMESPACE

class Ui_iwakerake
{
public:
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    QWidget *plotButtonWidget;
    QVBoxLayout *verticalLayout;
    QFrame *plotFrame;
    QGridLayout *gridLayout_3;
    QwtPlot *qwtCurrentFlightConfig;
    QwtPlot *qwtFlightConfigHistory;
    QFrame *buttonFrame;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *checkVelocityPushButton;
    QPushButton *takeWindOffPushButton;
    QPushButton *takeWindOnPushButton;
    QWidget *sliderWidget;
    QGridLayout *gridLayout;
    QFrame *sliderFrame;
    QGridLayout *gridLayout_2;
    QLabel *ReynoldsNumber;
    QLabel *Velocity;
    QwtThermo *qwtReynolds;
    QwtThermo *qwtVelocity;

    void setupUi(QMainWindow *iwakerake)
    {
        if (iwakerake->objectName().isEmpty())
            iwakerake->setObjectName(QStringLiteral("iwakerake"));
        iwakerake->resize(1032, 492);
        iwakerake->setMaximumSize(QSize(1032, 492));
        centralWidget = new QWidget(iwakerake);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(4);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(4, 4, 4, 4);
        plotButtonWidget = new QWidget(centralWidget);
        plotButtonWidget->setObjectName(QStringLiteral("plotButtonWidget"));
        verticalLayout = new QVBoxLayout(plotButtonWidget);
        verticalLayout->setSpacing(1);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        plotFrame = new QFrame(plotButtonWidget);
        plotFrame->setObjectName(QStringLiteral("plotFrame"));
        plotFrame->setAutoFillBackground(false);
        plotFrame->setFrameShape(QFrame::StyledPanel);
        plotFrame->setFrameShadow(QFrame::Raised);
        gridLayout_3 = new QGridLayout(plotFrame);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gridLayout_3->setHorizontalSpacing(2);
        gridLayout_3->setVerticalSpacing(6);
        gridLayout_3->setContentsMargins(4, 4, 4, 4);
        qwtCurrentFlightConfig = new QwtPlot(plotFrame);
        qwtCurrentFlightConfig->setObjectName(QStringLiteral("qwtCurrentFlightConfig"));

        gridLayout_3->addWidget(qwtCurrentFlightConfig, 0, 0, 1, 1);

        qwtFlightConfigHistory = new QwtPlot(plotFrame);
        qwtFlightConfigHistory->setObjectName(QStringLiteral("qwtFlightConfigHistory"));
        qwtFlightConfigHistory->setFrameShape(QFrame::NoFrame);
        qwtFlightConfigHistory->setFrameShadow(QFrame::Plain);
        qwtFlightConfigHistory->setLineWidth(1);

        gridLayout_3->addWidget(qwtFlightConfigHistory, 0, 1, 1, 1);


        verticalLayout->addWidget(plotFrame);

        buttonFrame = new QFrame(plotButtonWidget);
        buttonFrame->setObjectName(QStringLiteral("buttonFrame"));
        buttonFrame->setFrameShape(QFrame::StyledPanel);
        buttonFrame->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(buttonFrame);
        horizontalLayout_2->setSpacing(10);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(6, 6, 6, 6);
        checkVelocityPushButton = new QPushButton(buttonFrame);
        checkVelocityPushButton->setObjectName(QStringLiteral("checkVelocityPushButton"));
        QFont font;
        font.setPointSize(9);
        font.setBold(true);
        font.setWeight(75);
        checkVelocityPushButton->setFont(font);

        horizontalLayout_2->addWidget(checkVelocityPushButton);

        takeWindOffPushButton = new QPushButton(buttonFrame);
        takeWindOffPushButton->setObjectName(QStringLiteral("takeWindOffPushButton"));
        takeWindOffPushButton->setFont(font);

        horizontalLayout_2->addWidget(takeWindOffPushButton);

        takeWindOnPushButton = new QPushButton(buttonFrame);
        takeWindOnPushButton->setObjectName(QStringLiteral("takeWindOnPushButton"));
        takeWindOnPushButton->setFont(font);

        horizontalLayout_2->addWidget(takeWindOnPushButton);


        verticalLayout->addWidget(buttonFrame);


        horizontalLayout->addWidget(plotButtonWidget);

        sliderWidget = new QWidget(centralWidget);
        sliderWidget->setObjectName(QStringLiteral("sliderWidget"));
        gridLayout = new QGridLayout(sliderWidget);
        gridLayout->setSpacing(1);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(1, 1, 1, 0);
        sliderFrame = new QFrame(sliderWidget);
        sliderFrame->setObjectName(QStringLiteral("sliderFrame"));
        sliderFrame->setFrameShape(QFrame::StyledPanel);
        sliderFrame->setFrameShadow(QFrame::Raised);
        gridLayout_2 = new QGridLayout(sliderFrame);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setHorizontalSpacing(6);
        gridLayout_2->setVerticalSpacing(0);
        gridLayout_2->setContentsMargins(4, 4, 6, 4);
        ReynoldsNumber = new QLabel(sliderFrame);
        ReynoldsNumber->setObjectName(QStringLiteral("ReynoldsNumber"));
        ReynoldsNumber->setFont(font);

        gridLayout_2->addWidget(ReynoldsNumber, 1, 1, 1, 1, Qt::AlignRight);

        Velocity = new QLabel(sliderFrame);
        Velocity->setObjectName(QStringLiteral("Velocity"));
        Velocity->setFont(font);
        Velocity->setLayoutDirection(Qt::LeftToRight);

        gridLayout_2->addWidget(Velocity, 1, 2, 1, 1, Qt::AlignRight);

        qwtReynolds = new QwtThermo(sliderFrame);
        qwtReynolds->setObjectName(QStringLiteral("qwtReynolds"));
        QFont font1;
        font1.setPointSize(7);
        qwtReynolds->setFont(font1);
        qwtReynolds->setLowerBound(600000);
        qwtReynolds->setUpperBound(1e+06);
        qwtReynolds->setScaleMaxMinor(5);
        qwtReynolds->setScaleStepSize(40000);
        qwtReynolds->setAlarmEnabled(false);
        qwtReynolds->setSpacing(1);
        qwtReynolds->setBorderWidth(0);
        qwtReynolds->setPipeWidth(10);

        gridLayout_2->addWidget(qwtReynolds, 0, 1, 1, 1);

        qwtVelocity = new QwtThermo(sliderFrame);
        qwtVelocity->setObjectName(QStringLiteral("qwtVelocity"));
        qwtVelocity->setFont(font1);
        qwtVelocity->setLowerBound(0);
        qwtVelocity->setUpperBound(120);
        qwtVelocity->setScaleMaxMinor(5);
        qwtVelocity->setScaleStepSize(10);
        qwtVelocity->setSpacing(1);
        qwtVelocity->setBorderWidth(0);

        gridLayout_2->addWidget(qwtVelocity, 0, 2, 1, 1);


        gridLayout->addWidget(sliderFrame, 0, 0, 1, 1);


        horizontalLayout->addWidget(sliderWidget);

        iwakerake->setCentralWidget(centralWidget);

        retranslateUi(iwakerake);

        QMetaObject::connectSlotsByName(iwakerake);
    } // setupUi

    void retranslateUi(QMainWindow *iwakerake)
    {
        iwakerake->setWindowTitle(QApplication::translate("iwakerake", "iwakerake", 0));
        checkVelocityPushButton->setText(QApplication::translate("iwakerake", "Check Velocity", 0));
        takeWindOffPushButton->setText(QApplication::translate("iwakerake", "Take Wind Off", 0));
        takeWindOnPushButton->setText(QApplication::translate("iwakerake", "Take Wind On", 0));
        ReynoldsNumber->setText(QApplication::translate("iwakerake", "RE", 0));
        Velocity->setText(QApplication::translate("iwakerake", "V", 0));
    } // retranslateUi

};

namespace Ui {
    class iwakerake: public Ui_iwakerake {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_IWAKERAKE_H
