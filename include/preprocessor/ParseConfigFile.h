//
// Created by unoah on 02/12/18.
//

#ifndef IWAKERAKE_PARSECONFIGFILE_H
#define IWAKERAKE_PARSECONFIGFILE_H


#include "Preprocessor.h"

// Aircraft Parameters
struct AircraftParameters{
    double weight;              // Weight of the model
    double surfaceArea;         // Surface Area (ft^2)
    double alpha;               // Angle of attack
    double chord;               // Airfoil chord length
    double thicknessRatio;      // Thickness Ratio
};

// Wakerake parameters
struct WakeRakeParameters{
    double xi;                     // Distance behind the trailing edge wrt chord
    double ksi;                    // Distance behind the trailing edge wrt chord
    double zetaRake;               // Rake height non dimensionalized with chord
    double tubeInnerRadius;        // [ft, 1/32"]
    double tubeOuterRadius;        // [ft, 1/16"]
    double rakeTubesLength;        // Length of rake tubes (ft)
};

// Test parameters
struct TestParameters {
    int maxNumOfIterations;                     // max. number of iterations
    int numOfSpanwiseStations;                  // Number of spanwise test points
    double correctionFactorInitialValue;        // Correction factor initial value
};

// Environmental and thermodynamic conditions
struct AmbientParameters {
    double gasConstant;                // Perfect Gas Constant (English Units)
    double specificHeatRatio;        // Ratio of air specific heats
};

// DataRedux variables
struct DataReduxParameters {
    double cdEst;                  // Initial guess at "cd" used to begin data reduction iteration
};

class ParseConfigFile : public Preprocessor {
public:
    ParseConfigFile();
    virtual ~ParseConfigFile();


    void setAircraftParameters();
    AircraftParameters getAircraftParameters();

    void setWakeRakeParameters();
    WakeRakeParameters getWakeRakeParameters();

    void setTestParameters();
    TestParameters getTestParameters();

    void setAmbientParameters();
    AmbientParameters getAmbientParameters();

    void setDataReduxParameters();
    DataReduxParameters getDataReduxParameters();

private:
    AircraftParameters aircraftParameters;
    WakeRakeParameters wakeRakeParameters;
    TestParameters testParameters;
    DataReduxParameters dataReduxParameters;
    AmbientParameters ambientParameters;
};

#endif //IWAKERAKE_PARSECONFIGFILE_H
