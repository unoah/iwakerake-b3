//
// Created by unoah on 13/11/18.
//

#ifndef IWAKERAKE_PREPROCESSOR_H
#define IWAKERAKE_PREPROCESSOR_H

#include <libconfig.h++>

using namespace libconfig;

class Preprocessor {
public:
    Preprocessor();
    virtual ~Preprocessor();

    int loadConfigFile();

protected:
    Config iwakerakeConfig;
};


#endif //IWAKERAKE_PREPROCESSOR_H
