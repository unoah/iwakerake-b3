/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   GenerateFileName.h
 * Author: unoah
 *
 * Created on December 22, 2017, 10:46 PM
 */

#ifndef GENERATEFILENAME_H
#define GENERATEFILENAME_H

#include <string>

class GenerateFileName {
public:
    GenerateFileName();
    GenerateFileName(const GenerateFileName& orig);
    virtual ~GenerateFileName();

    std::string fileNamePrefix(const char srcFcn[]);
    std::string fileNameSuffix();
    std::string velocityToString(double flightVelocity);
    std::string fileNameUID();
private:
    std::string _filePrefixStr;
    std::string _fileSuffix;
    std::string _flightSpeed;
    std::string _stringUID;

};

#endif /* GENERATEFILENAME_H */

