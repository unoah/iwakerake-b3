//
// Created by unoah on 02/12/18.
//

#include "include/preprocessor/ParseConfigFile.h"

ParseConfigFile::ParseConfigFile() {
    this->loadConfigFile();
}

ParseConfigFile::~ParseConfigFile() {}

void ParseConfigFile::setAircraftParameters() {
    aircraftParameters.weight = iwakerakeConfig.lookup("AircraftParameters.weight");
    aircraftParameters.surfaceArea = iwakerakeConfig.lookup("AircraftParameters.surfaceArea");
    aircraftParameters.alpha = iwakerakeConfig.lookup("AircraftParameters.alpha");
    aircraftParameters.chord = iwakerakeConfig.lookup("AircraftParameters.chord");
    aircraftParameters.thicknessRatio = iwakerakeConfig.lookup("AircraftParameters.thicknessRatio");
    this->aircraftParameters = aircraftParameters;
}

AircraftParameters ParseConfigFile::getAircraftParameters() {
    return this->aircraftParameters;
}

void ParseConfigFile::setWakeRakeParameters() {
    wakeRakeParameters.xi = iwakerakeConfig.lookup("WakeRakeParameters.xi");
    wakeRakeParameters.ksi = iwakerakeConfig.lookup("WakeRakeParameters.ksi");
    wakeRakeParameters.zetaRake = iwakerakeConfig.lookup("WakeRakeParameters.zetaRake");
    wakeRakeParameters.tubeInnerRadius = iwakerakeConfig.lookup("WakeRakeParameters.tubeInnerRadius");
    wakeRakeParameters.tubeOuterRadius = iwakerakeConfig.lookup("WakeRakeParameters.tubeOuterRadius");
    wakeRakeParameters.rakeTubesLength = iwakerakeConfig.lookup("WakeRakeParameters.rakeTubesLength");
    this->wakeRakeParameters = wakeRakeParameters;
}

WakeRakeParameters ParseConfigFile::getWakeRakeParameters() {
    return this->wakeRakeParameters;
}

void ParseConfigFile::setTestParameters() {
    testParameters.maxNumOfIterations =
            iwakerakeConfig.lookup("TestParameters.maxNumOfIterations");
    testParameters.correctionFactorInitialValue =
            iwakerakeConfig.lookup("TestParameters.correctionFactorInitialValue");
    testParameters.numOfSpanwiseStations =
            iwakerakeConfig.lookup("TestParameters.numOfSpanwiseStations");
    this->testParameters = testParameters;
}

TestParameters ParseConfigFile::getTestParameters() {
    return this->testParameters;
}

void ParseConfigFile::setAmbientParameters() {
    ambientParameters.gasConstant = iwakerakeConfig.lookup("AmbientParameters.gasConstant");
    ambientParameters.specificHeatRatio = iwakerakeConfig.lookup("AmbientParameters.specificHeatRatio");
    this->ambientParameters = ambientParameters;
}

AmbientParameters ParseConfigFile::getAmbientParameters() {
    return this->ambientParameters;
}

void ParseConfigFile::setDataReduxParameters() {
    dataReduxParameters.cdEst = iwakerakeConfig.lookup("DataReduxParameters.cdEst");
    this->dataReduxParameters = dataReduxParameters;
}

DataReduxParameters ParseConfigFile::getDataReduxParameters() {
    return this->dataReduxParameters;
}