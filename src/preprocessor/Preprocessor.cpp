//
// Created by unoah on 13/11/18.
//

#include "include/preprocessor/Preprocessor.h"

#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>
#include <stdexcept>
#include <libconfig.h++>

using namespace libconfig;

Preprocessor::Preprocessor() {
    // TODO Add constructor code
}

Preprocessor::~Preprocessor() { }

int Preprocessor::loadConfigFile() {
    const char *ptrToStartupConfigFile = "iwakerake.cfg";
    try {
        iwakerakeConfig.readFile(ptrToStartupConfigFile);
    }
    catch (const FileIOException &fioex) {
        std::cerr << "\nI/O error while reading file: iwakerake.cfg" << std::endl;
        std::cerr << "Please ensure the file exists.\n" << std::endl;
        return(EXIT_FAILURE);
    }
    catch (const ParseException &pex) {
        std::cerr << "Parse error in file: " << pex.getFile() << " at line: " << pex.getLine()
                  << " - " << pex.getError() << std::endl;
        return (EXIT_FAILURE);
    }
    return 0;
}
