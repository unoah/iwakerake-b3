/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   GenerateFileName.cpp
 * Author: unoah
 * 
 * Created on December 22, 2017, 10:46 PM
 */

#include "include/preprocessor/GenerateFileName.h"
#include <cstring>

#include <QThread>  // Required for qsrand

using namespace std;

GenerateFileName::GenerateFileName() {
}

GenerateFileName::GenerateFileName(const GenerateFileName& orig) {
}

GenerateFileName::~GenerateFileName() {
}

string GenerateFileName::fileNamePrefix(const char srcFcn[]) {
    char windOffSrc[] = "windOff";
    char reducedwindOnSrc[] = "reducedwindOn";
    char reducedMeanWindOnSrc[] = "reducedMeanWindOn";

    if (strcmp(srcFcn, windOffSrc) == 0) {
        char filePrefix[] = "windOff";
        string filePrefixStr = string(filePrefix) + "_";
        return filePrefixStr;
    } else if (strcmp(srcFcn, reducedwindOnSrc) == 0) {
        char filePrefix[] = "reducedWindOn";
        string filePrefixStr = string(filePrefix) + "_";
        return filePrefixStr;
    } else if (strcmp(srcFcn, reducedMeanWindOnSrc) == 0) {
        char filePrefix[] = "reducedMeanWindOn";
        string filePrefixStr = string(filePrefix) + "_";
        return filePrefixStr;
    }
    return (0);
}

string GenerateFileName::fileNameSuffix() {
    string fileSuffix = ".txt";
    return fileSuffix;
}

string GenerateFileName::velocityToString(double flightVelocity) {
    string flightSpeed = to_string((int) flightVelocity) + "fps_";
    return flightSpeed;
}

string GenerateFileName::fileNameUID() {
    // initialize random seed
    qsrand((unsigned) time(NULL));

    string stringUID;
    string stringBank = "ABCDEFG123456789";

    int i = 0;
    for (i = 1; i <= 6; i++) {
        stringUID += stringBank[rand() % stringBank.size()];
        // Maybe add 1 to the % letters_numbers.size()
    }
    // stringUID = "_" + stringUID;
    return stringUID;
}
