/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SaveData.cpp
 * Author: unoah
 * 
 * Created on December 14, 2017, 6:31 AM
 */

#include "include/postprocessor/SaveData.h"
#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

SaveData::SaveData() {
}

SaveData::SaveData(const SaveData& orig) {
}

SaveData::~SaveData() {
}

int SaveData::rawSensorHeader(char fileName[]) {
    const char *rawSensorDataFile = fileName;
    ofstream rawSensorFile(rawSensorDataFile, ios::app);
    if (rawSensorFile.is_open()) {
        rawSensorFile << "Free Stream" << string(3, ' ') << "Free Stream" << string(4, ' ')
                << "WakeRake" << string(4, ' ') << "Dynamic" << "\n"
                << "Temperature" << string(4, ' ') << "Pressure" << string(6, ' ')
                << "Pressure" << string(4, ' ') << "Pressure" << "\n"
                << "-----------" << string(3, ' ') << "-----------" << string(4, ' ')
                << "--------" << string(4, ' ') << "--------" << "\n";
        rawSensorFile.close();
    }/* Next command needs to be modified with appropriate message
         * and must be presented to the user/operator as a popup window.
         */
    else cout << "Unable to open file";
    return 0;
}

int SaveData::rawSensorData(double rawTemperature, double rawPressure,
        double rawRakePressure, double rawDynamicPressure, char fileName[]) {

    const char *rawSensorDataFile = fileName;
    freeStreamTemperature = rawTemperature;
    freeStreamPressure = rawPressure;
    wakeRakePressure = rawRakePressure;
    dynamicPressure = rawDynamicPressure;

    ofstream rawSensorFile(rawSensorDataFile, ios::app);
    if (rawSensorFile.is_open()) {
        rawSensorFile << fixed
                << setprecision(6) << freeStreamTemperature << string(4, ' ')
                << setprecision(6) << freeStreamPressure << string(4, ' ')
                << setprecision(6) << wakeRakePressure << string(4, ' ')
                << setprecision(6) << dynamicPressure << "\n";
        rawSensorFile.close();
    }/* Next command needs to be modified with appropriate message
         * and must be presented to the user/operator as a popup window.
         */
    else cout << "Unable to open file";
    return 0;
}

int SaveData::reducedCurrentFlightHeader(char fileName[]) {
    const char *currentFlightHeader = fileName;

    ofstream reducedData(currentFlightHeader, ios::app);
    if (reducedData.is_open()) {
        reducedData << "          " << string(6, ' ') << "Reynolds" << string(6, ' ')
                << "Free Stream" << string(5, ' ') << "Free Stream" << string(4, ' ')
                << "         " << string(4, ' ') << "         " << string(4, ' ')
                << "Free Stream" << string(4, ' ') << "Iteration" << string(4, ' ') << "\n"

                << "Test Point" << string(7, ' ') << "Number" << string(7, ' ')
                << "Temperature" << string(6, ' ') << "Pressure" << string(6, ' ')
                << "Drag Coef" << string(4, ' ') << "Lift Coef" << string(6, ' ')
                << "Velocity" << string(7, ' ') << "Count" << "\n"

                << "----------" << string(3, ' ') << "-------------" << string(4, ' ')
                << "-----------" << string(4, ' ') << "-------------" << string(2, ' ')
                << "-----------" << string(2, ' ') << "-----------" << string(2, ' ')
                << "-------------" << string(2, ' ') << "-----------" << "\n";
        reducedData.close();
    }/* Next command needs to be modified with appropriate message
         * and must be presented to the user/operator as a popup window.
         */
    else cout << "Unable to open file";
    return 0;
}

int SaveData::reducedCurrentFlightData(char fileName[], int spanWiseStation, double RE,
        double currentTemperature, double currentPressure, double cd, double cl,
        double V_inf, int iterCount) {

    const char *currentFlightData = fileName;
    int testPoint = spanWiseStation;
    double reynoldsNumber = RE;
    double freeStreamTemp = currentTemperature;
    double freeStreamPres = currentPressure;
    double dragCoeff = cd;
    double liftCoeff = cl;
    double freeStreamVelocity = V_inf;
    int iterationCount = iterCount;

    ofstream reducedData(currentFlightData, ios::app);
    if (reducedData.is_open()) {
        reducedData << fixed
                << setprecision(6) << string(4, ' ') << testPoint << string(8, ' ')
                << setprecision(6) << reynoldsNumber << string(5, ' ')
                << setprecision(6) << freeStreamTemp << string(5, ' ')
                << setprecision(6) << freeStreamPres << string(4, ' ')
                << setprecision(6) << dragCoeff << string(5, ' ')
                << setprecision(6) << liftCoeff << string(6, ' ')
                << setprecision(6) << freeStreamVelocity << string(8, ' ')
                << setprecision(6) << iterationCount << "\n";
        reducedData.close();
    }/* Next command needs to be modified with appropriate message
         * and must be presented to the user/operator as a popup window.
         */
    else cout << "Unable to open file";
    return 0;
}

int SaveData::reducedMeanFlightHeader(char fileName[]) {
    const char *meanFlightHeader = fileName;

    ofstream reducedMeanData(meanFlightHeader, ios::app);
    if (reducedMeanData.is_open()) {
        reducedMeanData << " Reynolds" << string(6, ' ')
                << "Free Stream" << string(5, ' ') << "Free Stream" << string(4, ' ')
                << "         " << string(4, ' ') << "         " << string(4, ' ')
                << "Free Stream" << "\n"

                << "  Number" << string(7, ' ')
                << "Temperature" << string(6, ' ') << "Pressure" << string(6, ' ')
                << "Drag Coef" << string(4, ' ') << "Lift Coef" << string(6, ' ')
                << "Velocity" << "\n"

                << "----------" << string(4, ' ') << "-------------" << string(3, ' ')
                << "-------------" << string(2, ' ') << "-----------" << string(2, ' ')
                << "-----------" << string(2, ' ') << "-------------" << string(2, ' ') << "\n";
        reducedMeanData.close();
    }        /* Next command needs to be modified with appropriate message
         * and must be presented to the user/operator as a popup window.
         */
    else cout << "Unable to open file";
    return 0;
}

int SaveData::reducedMeanFlightData(char fileName[], double RE, double currentTemperature,
        double currentPressure, double cd, double cl, double V_inf) {

    const char *meanFlightData = fileName;
    double reynoldsNumber = RE;
    double freeStreamTemp = currentTemperature;
    double freeStreamPres = currentPressure;
    double dragCoeff = cd;
    double liftCoeff = cl;
    double freeStreamVelocity = V_inf;

    ofstream reducedMeanData(meanFlightData, ios::app);
    if (reducedMeanData.is_open()) {
        reducedMeanData << fixed
                << setprecision(2) << reynoldsNumber << string(6, ' ')
                << setprecision(6) << freeStreamTemp << string(6, ' ')
                << setprecision(6) << freeStreamPres << string(4, ' ')
                << setprecision(6) << dragCoeff << string(5, ' ')
                << setprecision(6) << liftCoeff << string(6, ' ')
                << setprecision(6) << V_inf << "\n";
        reducedMeanData.close();
    }
        /* Next command needs to be modified with appropriate message
         * and must be presented to the user/operator as a popup window.
         */
    else cout << "Unable to open file";
    return 0;
}
