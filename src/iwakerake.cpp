/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   iwakerake.cpp
 * Author: unoah
 * 
 * Created on December 12, 2017, 11:55 PM
 */

#include "include/iwakerake.h"
#include "include/ui_iwakerake.h"

#include "include/processor/Sensors.h"
#include "include/processor/FlowFunctions.h"
#include "include/processor/DataRedux.h"
#include "include/preprocessor/GenerateFileName.h"
#include "include/postprocessor/SaveData.h"
#include "include/processor/Sleeper.h"
#include "include/preprocessor/Preprocessor.h"
#include "include/preprocessor/ParseConfigFile.h"

#include <qwt_thermo.h>
#include <qwt_plot_grid.h>
#include <qwt_symbol.h>

#include <qwt_plot.h>
#include <qwt_plot_curve.h>

#include <ctime>
#include <cmath>

#include <fstream>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cstring>

#include <unistd.h>

// Required for development/building on OpenSUSE
// Comment out for development/building on Debian/Ubuntu
//#include <QApplication>
//#include <QMainWindow>

// Required for development/building on Debian/Ubuntu
// Un-comment before building on Debian/Ubuntu or the b3 board
//#include <QtWidgets/QApplication>
//#include <QtWidgets/QMainWindow>
#include <QApplication>
#include <QMainWindow>

static const int ALPHA = 4; // Angle of attack
static const int MAX_ITER = 50; // max. number of iterations
static const int FACTOR = 4; // Correction factor initial value

static const double rakeTubesLength = 0.25; // Length of rake tubes (ft)
static const double KSI = 0.361; // Distance behind the trailing edge wrt chord

using namespace std;

string testFilePrefix;
string testFileSpeed;
string testFileUID;
string testFileSuffix;

iwakerake::iwakerake(QWidget *parent) :
QMainWindow(parent),
ui(new Ui::iwakerake) {
    ui->setupUi(this);
    setWindowTitle(tr("iWakeRake"));
    resize(800, 420);

    QwtPlotGrid *currentFlightGrid = new QwtPlotGrid;
    currentFlightGrid->enableX(true);
    //currentFlightGrid->enableXMin( true );
    currentFlightGrid->enableY(true);
    //currentFlightGrid->enableYMin( true );
    currentFlightGrid->setMajorPen(Qt::black, 0, Qt::DotLine);
    currentFlightGrid->setMinorPen(Qt::gray, 0, Qt::DotLine);
    currentFlightGrid->attach(ui->qwtCurrentFlightConfig);

    QwtPlotGrid *flightHistoryGrid = new QwtPlotGrid;
    flightHistoryGrid->enableX(true);
    //flightHistoryGrid->enableXMin( true );
    flightHistoryGrid->enableY(true);
    //flightHistoryGrid->enableYMin( true );
    flightHistoryGrid->setMajorPen(Qt::black, 0, Qt::DotLine);
    flightHistoryGrid->setMinorPen(Qt::gray, 0, Qt::DotLine);
    flightHistoryGrid->attach(ui->qwtFlightConfigHistory);

    ui->qwtCurrentFlightConfig->setAutoReplot();
    ui->qwtCurrentFlightConfig->setCanvasBackground(Qt::white);
    ui->qwtCurrentFlightConfig->setAxisFont(QwtPlot::yLeft, (QFont("Cursive", 7)));
    ui->qwtCurrentFlightConfig->setAxisFont(QwtPlot::xBottom, (QFont("Cantarell", 7)));
    //    ui->qwtCurrentFlightConfig->setAxisTitle(QwtPlot::yLeft, "c<sub>l<sub>");
    //    ui->qwtCurrentFlightConfig->setAxisTitle(QwtPlot::xBottom, "c<sub>d<sub>");
    ui->qwtCurrentFlightConfig->setAxisScale(QwtPlot::yLeft, 0.340, 0.400);
    //ui->qwtCurrentFlightConfig->setAxisScale(QwtPlot::yLeft, 0.280, 0.286);
    ui->qwtCurrentFlightConfig->setAxisScale(QwtPlot::xBottom, 0.0055, 0.0085);

    QwtText qwtCurrentFlightConfigTitle("Current Flight Config Plot");
    qwtCurrentFlightConfigTitle.setFont(QFont("Cantarell", 8));
    ui->qwtCurrentFlightConfig->setTitle(qwtCurrentFlightConfigTitle);

    QwtText qwtCurrentFlightConfigXBottomTitle("c<sub>d<sub>");
    qwtCurrentFlightConfigXBottomTitle.setFont(QFont("TeXGyreChorus", 12, -1, true));
    ui->qwtCurrentFlightConfig->setAxisTitle(QwtPlot::xBottom, qwtCurrentFlightConfigXBottomTitle);

    QwtText qwtCurrentFlightConfigYLeftTitle("c<sub>l<sub>");
    qwtCurrentFlightConfigYLeftTitle.setFont(QFont("TeXGyreChorus", 12, -1, true));
    ui->qwtCurrentFlightConfig->setAxisTitle(QwtPlot::yLeft, qwtCurrentFlightConfigYLeftTitle);

    ui->qwtFlightConfigHistory->setAutoReplot();
    ui->qwtFlightConfigHistory->setCanvasBackground(Qt::white);
    ui->qwtFlightConfigHistory->setAxisFont(QwtPlot::yLeft, (QFont("Cantarell", 7)));
    ui->qwtFlightConfigHistory->setAxisFont(QwtPlot::xBottom, (QFont("Cantarell", 7)));
    //    ui->qwtFlightConfigHistory->setAxisTitle(QwtPlot::yLeft, "c<sub>l<sub>");
    //    ui->qwtFlightConfigHistory->setAxisTitle(QwtPlot::xBottom, "c<sub>d<sub>");
    ui->qwtFlightConfigHistory->setAxisScale(QwtPlot::yLeft, 0.340, 0.400);
    //ui->qwtFlightConfigHistory->setAxisScale(QwtPlot::yLeft, 0.280, 0.286);
    ui->qwtFlightConfigHistory->setAxisScale(QwtPlot::xBottom, 0.0055, 0.0085);

    QwtText qwtFlightConfigHistoryTitle("Flight Config History Plot");
    qwtFlightConfigHistoryTitle.setFont(QFont("Cantarell", 8));
    ui->qwtFlightConfigHistory->setTitle(qwtFlightConfigHistoryTitle);

    QwtText qwtFlightConfigHistoryXBottomTitle("c<sub>d<sub>");
    qwtFlightConfigHistoryXBottomTitle.setFont(QFont("TeXGyreChorus", 12, -1, true));
    ui->qwtFlightConfigHistory->setAxisTitle(QwtPlot::xBottom, qwtFlightConfigHistoryXBottomTitle);

    QwtText qwtFlightConfigHistoryYLeftTitle("c<sub>l<sub>");
    qwtFlightConfigHistoryYLeftTitle.setFont(QFont("TeXGyreChorus", 12, -1, true));
    ui->qwtFlightConfigHistory->setAxisTitle(QwtPlot::yLeft, qwtFlightConfigHistoryYLeftTitle);
}

iwakerake::~iwakerake() {
    delete ui;
}

void iwakerake::on_takeWindOnPushButton_clicked() {
    ui->takeWindOnPushButton->setDown(true);
    ui->takeWindOnPushButton->setDisabled(true);
    
    /* Generate file name if trackHeader is 1, otherwise use use existing file.
     * Having this ensures measurement and data reduction across the entire wing 
     * is saved to a unique file as opposed to creating unique files for each 
     * spanwise station */
    int trackHeader = 1;

    // Testing Code
    ParseConfigFile iwakerakeConf;
    iwakerakeConf.setAircraftParameters();
    iwakerakeConf.setTestParameters();
    iwakerakeConf.setWakeRakeParameters();
    iwakerakeConf.setAmbientParameters();
    iwakerakeConf.setDataReduxParameters();

    // Aircraft Parameters
    double TC = iwakerakeConf.getAircraftParameters().thicknessRatio;
    double chord = iwakerakeConf.getAircraftParameters().chord;
    double weight = iwakerakeConf.getAircraftParameters().weight;
    double surfaceArea = iwakerakeConf.getAircraftParameters().surfaceArea;

    // Ambient Parameters
    double gasConst = iwakerakeConf.getAmbientParameters().gasConstant;
    double specificHeatRatio = iwakerakeConf.getAmbientParameters().specificHeatRatio;

    // Test Parameters
    int numOfSpanwiseStations = iwakerakeConf.getTestParameters().numOfSpanwiseStations;

    // Wake Rake Parameters
    double Xi = iwakerakeConf.getWakeRakeParameters().xi;
    double zetaRake = iwakerakeConf.getWakeRakeParameters().zetaRake;

    // Data Reduction Parameters
    double cdEst = iwakerakeConf.getDataReduxParameters().cdEst;

    double dragCoefPoint [numOfSpanwiseStations];
    double liftCoefPoint [numOfSpanwiseStations];

    double dragCoefCummulative = 0;
    double liftCoefCummulative = 0;
    double velocityCummulative = 0;
    double dynaPresCummulative = 0;
    double reynoldsCummulative = 0;
    double freeStreamTempCummulative = 0;

    double dragCoefHistory = 0;
    double liftCoefHistory = 0;
    double velocityHistory = 0;
    double dynaPresHistory = 0;
    double reynoldsHistory = 0;
    double freeStreamTempHistory = 0;

    // Initialize class used to save data
    SaveData saveReducedData;

    // Initialize class used to generate file names
    GenerateFileName testFile;

    // Insert Flight History Plot Curve
    QwtPlotCurve *flightHistoryPlot = new QwtPlotCurve();
    flightHistoryPlot->setPen(Qt::black, -1);

    QwtSymbol *flightHistorySymbol = new QwtSymbol();
    flightHistorySymbol->setStyle(QwtSymbol::XCross);
    flightHistorySymbol->setPen(QPen(Qt::red));
    //QBrush flightHistoryBrush(Qt::black);
    //flightHistorySymbol->setBrush(flightHistoryBrush);
    flightHistorySymbol->setSize(7);
    flightHistoryPlot->setSymbol(flightHistorySymbol);

    ui->qwtCurrentFlightConfig->detachItems(QwtPlotItem::Rtti_PlotCurve);
    ui->qwtCurrentFlightConfig->detachItems(QwtPlotItem::Rtti_PlotMarker);

    for (int spanWiseStation = 1; spanWiseStation <= numOfSpanwiseStations; spanWiseStation++) {

        // Insert Current Flight Plot Curve
        QwtPlotCurve *currentFlightPlot = new QwtPlotCurve();
        currentFlightPlot->setPen(Qt::black, -1);

        QwtSymbol *currentFlightSymbol = new QwtSymbol();
        currentFlightSymbol->setStyle(QwtSymbol::XCross);
        currentFlightSymbol->setPen(QPen(Qt::blue));
        //    QBrush currentFlightBrush(Qt::black);
        //    currentFlightSymbol->setBrush(currentFlightBrush);
        currentFlightSymbol->setSize(7);
        currentFlightPlot->setSymbol(currentFlightSymbol);

        Sensors dummySensor;
        double freeStreamTemp = dummySensor.getFreeStreamTemperature();
        double freeStreamPressure = dummySensor.getFreeStreamPressure();
        double wakeRakePressure = dummySensor.getWakeRakePressure();
        double dynamicPressure = dummySensor.getDynamicPressure();
        dummySensor.~Sensors();

        FlowFunctions dummyFlow;
        double freeStreamDensity = dummyFlow.getFreeStreamDensity(freeStreamPressure, freeStreamTemp, gasConst);
        double freeStreamVelocity = dummyFlow.getFreeStreamVelocity(dynamicPressure, freeStreamDensity);
        double speedOfSound = dummyFlow.getSpeedOfSound(gasConst, specificHeatRatio, freeStreamTemp);
        double MachNo = dummyFlow.getMachNumber(freeStreamVelocity, speedOfSound);
        double absViscosity = dummyFlow.getAbsoluteViscosity(freeStreamTemp);
        double reynoldsNumber = dummyFlow.getReynoldsNumber(absViscosity, chord, freeStreamDensity, freeStreamVelocity);
        double totalPressure = dummyFlow.getTotalPressure(freeStreamPressure, dynamicPressure);
        dummyFlow.~FlowFunctions();

        ui->qwtReynolds->setValue(reynoldsNumber);
        ui->qwtReynolds->show();
        ui->qwtReynolds->repaint();

        ui->qwtVelocity->setValue(freeStreamVelocity);
        ui->qwtVelocity->show();
        ui->qwtVelocity->repaint();

        if (trackHeader == 1) {
            testFilePrefix = testFile.fileNamePrefix("reducedwindOn");
            testFileSpeed = testFile.velocityToString(freeStreamVelocity);
            testFileUID = testFile.fileNameUID();
            testFileSuffix = testFile.fileNameSuffix();
        }
        string fileNameStr = testFilePrefix + testFileSpeed + testFileUID + testFileSuffix;
        char fileName[fileNameStr.size() + 1]; //as 1 char space for null is also required
        strcpy(fileName, fileNameStr.c_str());

        if (trackHeader == 1) {
            saveReducedData.reducedCurrentFlightHeader(fileName);
        }
        trackHeader = 2;

        // Begin iteration loop to calculate drag coefficient
        double cdNew;
        double cdOld;
        double clNew;

        cdNew = cdEst;
        cdOld = 1.00;

        int iterationCount = 0;

        while (abs((cdOld - cdNew) / cdNew) >= 0.00007) {
            cdOld = cdNew;
            DataRedux reduceData;
            double etaWake = reduceData.getEtaWake(reynoldsNumber, TC, Xi, cdNew);
            double zetaWake = reduceData.getZetaWake(reynoldsNumber, TC, Xi, cdNew, chord);
            double dimensionlessPressure = reduceData.getDimensionlessPressure(reynoldsNumber, TC, Xi);
            double pressureDeficitCorrection = reduceData.getPressureDeficitCorrection(etaWake, zetaRake, wakeRakePressure, dynamicPressure);
            double liftCoefficient = reduceData.getLiftCoefficient(weight, surfaceArea, freeStreamDensity, freeStreamVelocity);
            clNew = liftCoefficient;
            reduceData.~DataRedux();

            DataRedux F(dimensionlessPressure, zetaWake, etaWake, MachNo);
            double integratingFactor = F.getIntegratingFactor();
            double uncorrectedDragCoefficient = reduceData.getUncorrectedDragCoefficient(integratingFactor, zetaRake, chord, pressureDeficitCorrection, dynamicPressure);
            double compressibilityCorrection = F.getCompressibilityCorrection();
            double correctedDragCoefficient = uncorrectedDragCoefficient * compressibilityCorrection;
            cdNew = correctedDragCoefficient;
            F.~DataRedux();

            iterationCount = iterationCount + 1;
        }

        saveReducedData.reducedCurrentFlightData(fileName, spanWiseStation, reynoldsNumber,
                freeStreamTemp, freeStreamPressure, cdNew, clNew, freeStreamVelocity, iterationCount);

        dragCoefPoint[spanWiseStation] = cdNew;
        liftCoefPoint[spanWiseStation] = clNew;

        dragCoefCummulative = dragCoefCummulative + cdNew;
        liftCoefCummulative = liftCoefCummulative + clNew;
        velocityCummulative = velocityCummulative + freeStreamVelocity;
        dynaPresCummulative = dynaPresCummulative + freeStreamPressure;
        reynoldsCummulative = reynoldsCummulative + reynoldsNumber;
        freeStreamTempCummulative = freeStreamTempCummulative + freeStreamTemp;

        Sleeper::sleep(1); // Simulated delay. Actual delay will exist as rake moves to next spanwise station

        QPolygonF currentFlightPoints;
        currentFlightPoints << QPointF(dragCoefPoint[spanWiseStation], liftCoefPoint[spanWiseStation]);
        currentFlightPlot->setSamples(currentFlightPoints);
        currentFlightPlot->attach(ui->qwtCurrentFlightConfig);
        ui->qwtCurrentFlightConfig->show();
        ui->qwtCurrentFlightConfig->replot();
    }

    dragCoefHistory = (dragCoefCummulative / numOfSpanwiseStations);
    liftCoefHistory = (liftCoefCummulative / numOfSpanwiseStations);
    velocityHistory = (velocityCummulative / numOfSpanwiseStations);
    dynaPresHistory = (dynaPresCummulative / numOfSpanwiseStations);
    reynoldsHistory = (reynoldsCummulative / numOfSpanwiseStations);
    freeStreamTempHistory = (freeStreamTempCummulative / numOfSpanwiseStations);

    trackHeader = 1;
    /* Generate file name if trackHeader is 1, otherwise use use existing file.
     * Having this ensures measurement and data reduction across the entire wing
     * is saved to a unique file as opposed to creating unique files for each 
     * spanwise station*/
    testFilePrefix = testFile.fileNamePrefix("reducedMeanWindOn");
    testFileSpeed = testFile.velocityToString(velocityHistory);
    testFileSuffix = testFile.fileNameSuffix();

    string fileNameStr = testFilePrefix + testFileSpeed + testFileUID + testFileSuffix;
    char fileName[fileNameStr.size() + 1]; //as 1 char space for null is also required
    strcpy(fileName, fileNameStr.c_str());

    if (trackHeader == 1) {
        saveReducedData.reducedMeanFlightHeader(fileName);
    };
    trackHeader = 2;

    saveReducedData.reducedMeanFlightData(fileName, reynoldsHistory, freeStreamTempHistory,
            dynaPresHistory, dragCoefHistory, liftCoefHistory, velocityHistory);

    QPolygonF flightConfigHistoryPoints;
    flightConfigHistoryPoints << QPointF(dragCoefHistory, liftCoefHistory); // will be adding actual data points as required later. for now using dummy numbers
    flightHistoryPlot->setSamples(flightConfigHistoryPoints);
    flightHistoryPlot->attach(ui->qwtFlightConfigHistory);
    ui->qwtFlightConfigHistory->show();

    ui->takeWindOnPushButton->setDown(false);
    ui->takeWindOnPushButton->setDisabled(false);

    // Reset drag coefficient data used in flight history plot
    dragCoefCummulative = 0;
    liftCoefCummulative = 0;

    dragCoefHistory = 0;
    liftCoefHistory = 0;
    
    saveReducedData.~SaveData();
    testFile.~GenerateFileName();
}
