//
// Created by unoah on 13/11/18.
//

#include "include/processor/Sleeper.h"

void Sleeper::usleep(unsigned long usecs) {
    QThread::usleep(usecs);
}

void Sleeper::msleep(unsigned long msecs) {
    QThread::msleep(msecs);
}

void Sleeper::sleep(unsigned long secs) {
    QThread::sleep(secs);
}