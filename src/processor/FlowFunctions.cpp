/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FlowFunctions.cpp
 * Author: unoah
 * 
 * Created on February 4, 2017, 11:20 AM
 */

#include "include/processor/FlowFunctions.h"
#include <ctime>
#include <cmath>

FlowFunctions::FlowFunctions() {
}

FlowFunctions::FlowFunctions(const FlowFunctions& orig) {
}

FlowFunctions::~FlowFunctions() {
}

double FlowFunctions::getReynoldsNumber(double absViscosity, double airfoilChord,
        double freeStreamDensity, double freeStreamVelocity) {
    reynoldsNumber = ((freeStreamDensity * freeStreamVelocity * airfoilChord) / absViscosity);
    return reynoldsNumber;
}

double FlowFunctions::getFreeStreamDensity(double freeStreamPressure,
        double freeStreamTemperature, double gasConst) {
    freeStreamDensity = (freeStreamPressure / (freeStreamTemperature * gasConst));
    return freeStreamDensity;
}

double FlowFunctions::getFreeStreamVelocity(double dynamicPressure,
        double freeStreamDensity) {
    freeStreamVelocity = sqrt((2 * dynamicPressure) / freeStreamDensity);
    return freeStreamVelocity;
}

double FlowFunctions::getSpeedOfSound(double gasConstant, double specificHeatRatio,
        double freeStreamTemperature) {
    speedOfSound = sqrt(specificHeatRatio * gasConstant * freeStreamTemperature);
    return speedOfSound;
}

double FlowFunctions::getMachNumber(double freeStreamVelocity, double speedOfSound) {
    freeStreamMachNumber = (freeStreamVelocity / speedOfSound);
    return freeStreamMachNumber;
}

double FlowFunctions::getAbsoluteViscosity(double freeStreamTemperature) {
    absoluteViscosity = (pow(10.0, -10) * (0.317)) * pow(freeStreamTemperature, 1.5)
            * (734.7 / (freeStreamTemperature + 216));
    return absoluteViscosity;
}

double FlowFunctions::getTotalPressure(double staticPressure, double dynamicPressure) {
    totalPressure = (staticPressure - dynamicPressure);
    return totalPressure;
}