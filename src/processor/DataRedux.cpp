/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DataRedux.cpp
 * Author: unoah
 * 
 * Created on February 4, 2017, 11:16 AM
 */

#include "include/processor/DataRedux.h"
#include <iostream>
#include <iomanip>
#include <fstream>

#include <ctime>
#include <cmath>
#include <gsl/gsl_integration.h>

using namespace std;

static const double PI = 3.14159654;
static const double SPECIFIC_HEAT_RATIO = 1.4; // Gamma
static const double RAKE_TUBE_INNER_RADIUS = .0013; // [ft, 1/32"]
static const double RAKE_TUBE_OUTER_RADIUS = .0026; // [ft, 1/16"]

double _dimensionlessPressure;
//double _dimensionlessWakeHeight;
double _zetaWake;
double _etaWake;
double _MachNo;

DataRedux::DataRedux() {
}

DataRedux::DataRedux(const DataRedux& orig) {
}

//DataRedux::DataRedux(double dimensionlessPressure, double dimensionlessWakeHeight,
//        double etaWake) {
//    _dimensionlessPressure = dimensionlessPressure;
//    _dimensionlessWakeHeight = dimensionlessWakeHeight;
//    _etaWake = etaWake;
//}

DataRedux::DataRedux(double dimensionlessPressure, double zetaWake, double etaWake,
        double MachNo) {
    _dimensionlessPressure = dimensionlessPressure;
    _zetaWake = zetaWake;
    _etaWake = etaWake;
    _MachNo = MachNo;
}

DataRedux::~DataRedux() {
}

double DataRedux::getThicknessRatio() {
    thicknessRatio = 0.1425;
    return thicknessRatio;
}

double DataRedux::getEtaWake(double RE, double TC, double Xi, double cd) {
    double A;
    double B;
    double C;

    A = ((-1.08e-12) * pow(RE, 2));
    B = (((3.35e-6) * RE));
    C = (sqrt(cd * TC) / (Xi + 0.3));
    etaWake = (((A + B) + 4.15) * C);
    return etaWake;
}

double DataRedux::getZetaWake(double RE, double TC, double Xi, double cd,
        double airfoilChord) {
    double A;
    double B;
    double C;

    A = ((0.34e-12) * pow(RE, 2));
    B = (((1.07e-6) * RE));
    C = (sqrt(cd * TC * (Xi + 0.15)));
    //zetaWake = (airfoilChord * (((A - B) + 3.21)* C));
    zetaWake = ((((A - B) + 3.21) * C));
    return zetaWake;
}

double DataRedux::getgetDimensionlessRakeHeight() {
    dimensionlessRakeHeight = 0.4444;
    return dimensionlessRakeHeight;
    /* This variable is called w in original MATLAB code.
     * A better approach is to get the airfoil chord length and
     * non-dimensionalize it in this function.
     * This approach is the reason this function was created as opposed
     * to having the numeric value left as a static constant.
     */
}

double DataRedux::getDimensionlessPressure(double RE, double TC, double Xi) {
    double A;
    double B;

    A = (((-1.33e-6) * RE) + 4.36);
    B = ((TC) / (pow((0.77 + (3.1 * Xi)), 2)));
    dimensionlessPressure = (A * B);
    return dimensionlessPressure;
}

double DataRedux::getPressureDeficitCorrection(double etaWake,
        double zetaRake, double wakeRakePressure, double dynamicPressure) {
    double A;
    double B;

    A = ((2 * etaWake * dynamicPressure) / (zetaRake));
    B = ((2 * 0.131 * RAKE_TUBE_OUTER_RADIUS) + (2 * 0.0821 * RAKE_TUBE_INNER_RADIUS));
    pressureDeficitCorrection = (wakeRakePressure + (A * B));
    return pressureDeficitCorrection;
}

double DataRedux::getLiftCoefficient(double WEIGHT, double SURFACE_AREA,
            double freeStreamDensity, double freeStreamVelocity){
    double A;
    double B;
    
    A = (2 * WEIGHT);
    B = ((freeStreamDensity * pow(freeStreamVelocity, 2) * SURFACE_AREA));
    liftCoefficient = (A / B);
    return liftCoefficient;
}

double DataRedux::getUncorrectedDragCoefficient(double integratingFactor,
        double dimensionlessRakeHeight, double airfoilChord,
        double pressureDeficitCorrection, double dynamicPressure) {
    double A;
    double B;

    A = (integratingFactor * (dimensionlessRakeHeight / airfoilChord));
    B = (pressureDeficitCorrection / dynamicPressure);
    uncorrectedDragCoefficient = (A * B);
    return uncorrectedDragCoefficient;
}

//double DataRedux::getUncorrectedDragCoefficient(double integratingFactor,
//        double dimensionlessRakeHeight, double airfoilChord,
//        double totalPressure, double wakeRakePressure,
//        double dynamicPressure) {
//    double A;
//    double B;
//
//    A = (integratingFactor * (dimensionlessRakeHeight / airfoilChord));
//    B = ((totalPressure - wakeRakePressure) / dynamicPressure);
//    uncorrectedDragCoefficient = (A * B);
//    return uncorrectedDragCoefficient;
//}

double DataRedux::getIntegratingFactor(void) {
    gsl_integration_workspace *wSpacePtr = gsl_integration_workspace_alloc(1000);
    double lowerLimit = 0.0;
    double upperLimit = _zetaWake;
    double absoluteError = 1.0e-8;
    double relativeError = 1.0e-8;
    double result;
    double error;

    double alpha = 1.0; // parameter in integrand

    gsl_function _integratingFactorEquation;
    void *params_ptr = &alpha;

    //_integratingFactorFunction.function = &getIntegratingFactorFunction;
    _integratingFactorEquation.function = &DataRedux::integratingFactorEquation;
    _integratingFactorEquation.params = params_ptr;

    gsl_integration_qags(&_integratingFactorEquation, lowerLimit, upperLimit,
            absoluteError, relativeError, 1000, wSpacePtr, &result, &error);

    //    gsl_integration_qags(&_integratingFactorFunction, lowerLimit, y,
    //            absoluteError, relativeError, 1000, wSpacePtr, &result, &error);

    /* Results of the integration can be returned here. Likely have to use a 
     * pointer that allows that value to be read and stored before the
     * releasing used memory as indicated below. It might be possible to
     * release the used memory by other methods.
     */

    gsl_integration_workspace_free(wSpacePtr);

    double F = (4 * result) / (_etaWake * _zetaWake);
    return F;
}

double DataRedux::integratingFactorEquation(double y, void *params) {
    // Recover alpha from the passed params pointer
    double alpha = *(double *) params;
    double integratingFactorEquation = (sqrt(1 - _dimensionlessPressure -
            (_etaWake * pow(cos((PI * y) / (_zetaWake)), 2)))) *
            (1 - (sqrt(1 - (_etaWake * pow(cos((PI * y) / (_zetaWake)), 2)))));
    return integratingFactorEquation;
}

double DataRedux::getCompressibilityCorrection(void) {
    gsl_integration_workspace *wSpacePtr = gsl_integration_workspace_alloc(1000);
    double lowerLimit = 0.0;
    double upperLimit = _zetaWake;
    double absoluteError = 1.0e-8;
    double relativeError = 1.0e-8;
    double result;
    double error;

    double alpha = 1.0; // parameter in integrand

    gsl_function _compressibilityCorrectionEquation;
    void *params_ptr = &alpha;

    _compressibilityCorrectionEquation.function = &DataRedux::compressibilityCorrectionEquation;
    _compressibilityCorrectionEquation.params = params_ptr;

    gsl_integration_qags(&_compressibilityCorrectionEquation, lowerLimit, upperLimit,
            absoluteError, relativeError, 1000, wSpacePtr, &result, &error);

    gsl_integration_workspace_free(wSpacePtr);

    double _compressibilityCorrection;
    _compressibilityCorrection = 1 + ((pow(_MachNo, 2) / 8) * result);
    return _compressibilityCorrection;
}

double DataRedux::compressibilityCorrectionEquation(double y, void *params) {
    // Recover alpha from the passed params pointer
    double alpha = *(double *) params;

    double compressibilityCorrectionEquation = (2 * _dimensionlessPressure) -
            ((2 * SPECIFIC_HEAT_RATIO) - 1) + (2 * _etaWake * (pow(cos((PI * y) / (_zetaWake)), 2))) -
            (((2 * SPECIFIC_HEAT_RATIO) - 1)*(sqrt(1 - (_etaWake * (pow(cos((PI * y) / (_zetaWake)), 2))))));
    return compressibilityCorrectionEquation;
}