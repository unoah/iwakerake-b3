/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Sensors.cpp
 * Author: unoah
 * 
 * Created on February 4, 2017, 8:16 PM
 */

#include "include/processor/Sensors.h"
#include <ctime>
#include <cmath>
#include <cstdlib>

Sensors::Sensors() {
}

Sensors::Sensors(const Sensors& orig) {
}

Sensors::~Sensors() {
}

double Sensors::getDynamicPressure(){
    double pressureUpperLimit = 30.00;
    double pressureLowerLimit = 10.2200;
    srand(time(0));
    dummyDynamicPressure = (pressureLowerLimit + fmod(((double)rand()/RAND_MAX),
            (pressureUpperLimit-pressureLowerLimit + 1)));
    return dummyDynamicPressure;
}

double Sensors::getFreeStreamPressure(){
    double pressureUpperLimit = 2030.00;
    double pressureLowerLimit = 2024.00;
    srand(time(0));
    dummyFreeStreamPressure = (pressureLowerLimit + fmod(((double)rand()/RAND_MAX),
            (pressureUpperLimit-pressureLowerLimit + 1)));
    return dummyFreeStreamPressure;
}

double Sensors::getWakeRakePressure(){
    double pressureUpperLimit = 0.3500;
    double pressureLowerLimit = 0.2200;
    srand(time(0));
    dummyWakeRakePressure = (pressureLowerLimit + fmod(((double)rand()/RAND_MAX),
            (pressureUpperLimit-pressureLowerLimit + 0.05)));
    return dummyWakeRakePressure;
}

double Sensors::getFreeStreamTemperature(){
    double temperatureUpperLimit = 634.00;
    double temperatureLowerLimit = 528.00;
    srand(time(0));
    dummyFreeStreamTemperature = (temperatureLowerLimit + fmod(((double)rand()/RAND_MAX),
            (temperatureUpperLimit-temperatureLowerLimit + 1)));
    return dummyFreeStreamTemperature;
}