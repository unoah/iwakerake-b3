/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: unoah
 * 
 * Created on December 12, 2017, 11:55 PM
 */

#include "../include/iwakerake.h"
#include <QApplication>

int main(int argc, char *argv[]) {
    /* QApplication::setStyle(). This is used to set the style of
     * the entire application. setStyle() accepts any one of the
     * following: "motif", "cde", "plastique" and "cleanlooks".
     * */
    QApplication::setStyle("cde");
    QApplication a(argc, argv);
    iwakerake w;
    w.show();

    return a.exec();
}
